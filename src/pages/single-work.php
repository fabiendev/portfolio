<?php 
require_once('../functions/single-work-functions.php');
get_header();
?>
<script defer src="../assets/js/baguetteBox.min.js"></script>
<script defer src="../assets/js/gallery.js"></script>

<section class="work">
    <div class="work__container container flex-row ">
        <?php foreach ($singleWork as $value) { ?>
            <div class="work__thumbnail">
                <img src="../../src/assets/images/works/thumbnail/<?php echo $value['thumbnail'] ?>" alt="Visuel du projet" class="work__thumbnail-picture">      
            </div>
            <aside class="work__content">
                <h1 class="work__title title"><?php echo $value['name']; ?></h1>
                <div class="work__subtitle">
                    <h3 class="work__subtitle-title">Détail</h3>
                    <p class="work__subtitle-content"><?php echo $value['subtitle']; ?></p>
                </div>
                <div class="work__description">
                    <h3 class="work__description-title">Description du projet</h3>
                    <p class="work__description-content"><?php echo nl2br($value['description']); ?></p>
                </div>
                <hr class="work__separator separator">
                <div class="label flex-row flex-row--start">
                    <span class="label__categorie label-btn label-btn--xsmall <?php echo getCategorieColorBtn($value['categorie']); ?>"><?php echo $value['categorie']; ?></span>
                    <?php foreach (getTechnologiesInfos($value['id']) as $tech) { ?>
                        <span class="label__tag label-btn <?php echo getTechnologiesColors($tech['name']); ?> label-btn--xsmall"><?php echo $tech['name']; ?></span>
                    <?php } ?>
                </div>
                <hr class="work__separator separator">
                <div class="link flex-column">
                <?php if (!empty($value['gitlab'])) { ?>
                    <div class="link__item">
                        <a href="<?php echo $value['gitlab']; ?>" class="link__btn link__btn--gitlab">
                            <span class="link__text">Voir le code</span>
                            <i class="link__icon fab fa-gitlab"></i>
                        </a>
                    </div>
                <?php } ?>
                <?php if (!empty($value['url'])) { ?>
                    <div class="link__item">
                        <a href="<?php echo $value['url']; ?>" class="link__btn link__btn--url">
                            <span class="link__text">Voir le site</span>
                            <i class="link__icon fas fa-globe"></i>
                        </a>
                    </div>
                <?php } ?>
            </div>
            </aside> 
        <?php } ?>
    </div>
    <div class="screenshots container-full">
        <div class="screenshots__container">
            <h2 class="screenshots__title title">Captures d'écran</h2>
            <div class="gallery flex-column">
                <?php foreach ($gallery as $picture) { ?>
                    <div class="gallery__item">
                        <a href="../../src/assets/images/works/<?php echo $picture['project_name']; ?>/<?php echo $picture['url']; ?>" data-caption="<?php echo $picture['name']; ?>" class="gallery__link">
                            <img src="../../src/assets/images/works/<?php echo $picture['project_name']; ?>/<?php echo $picture['url']; ?>" alt="<?php echo $picture['name']; ?>" class="gallery__picture">
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>