<?php 
require_once('../functions/404-functions.php');
get_header(); 
?>

<div class="error-page container flex-column">
    <h1 class="error-page__title title">Oups !</h1>
    <h2 class="error-page__subtitle subtitle">Il semblerait que vous vous soyez trompé de sommet</h2>
    <a href="https://fabiendev.com" class="error-page__btn">
        <span class="error-page__btn-text">Revenir sur le bon chemin</span> 
        <i class="error-page__btn-icon fas fa-mountain"></i>
    </a>
</div> 
    

<?php get_footer(); ?>