<?php 
require_once('../functions/contact-functions.php');
get_header();
?>

<section class="contact container">
    <img src="../../src/assets/images/mountain.png" alt="Visuel montagne" class="contact__picture">
    <div class="notification">
        <?php if(!empty($_POST)) {
            $error = sendMail();
            if(isset($error)) {
                if($error) {
                    foreach($error as $value) { ?>
                    <div class="notification__error">
                        <span class="notification__error-message"><?= $value; ?></span>
                    </div>
                    <?php }
                } else { ?>
                    <div class="notification__confirm">
                        <span class="notification__confirm-message">Votre mail a bien été envoyé !<br>
                        Je reviendrai vers vous dès que possible.</span>
                    </div>
                <?php }
            }
        } ?>
    </div>
    <h2 class="contact__title title">Contact</h2>
    <p class="contact__subtitle subtitle">Vous pouvez me contacter en utilisant le formulaire ci-dessous.</p>
    <div class="contact__form">
        <form class="form" action="contact.php" method="post">
            <div class="form__group">
                <label for="email" class="form__label">Votre email</label>
                <input type="text" name="email" id="email" class="form__email" value="<?php if(isset($_POST["email"])) echo $_POST["email"] ?>" />
            </div>
            <div class="form__group">
                <label for="subject" class="form__label">Sujet</label>
                <input type="text" name="subject" id="subject" class="form__subject" value="<?php if(isset($_POST["subject"])) echo $_POST["subject"] ?>" />
            </div>
            <div class="form__group">
                <label for="message" class="form__label">Message</label>
                <textarea name="message" id="message" class="form__message" cols="40" rows="20"><?php if(isset($_POST["message"])) echo $_POST["message"] ?></textarea>
            </div>
            <input type="hidden" name="reason">
            <div class="form__group">
                <input type="submit" class="form__btn btn btn--mint btn--center" value="Envoyer" />
            </div>
        </form>
    </div>
</section>

<?php get_footer(); ?>