<?php 
require_once('../functions/works-functions.php');
get_header();
?>
<script defer src="../assets/js/filterTogg.js"></script>
<script defer src="../assets/js/tagsFilter.js"></script>

<section class="all-works container">
    <div class="all-works__head flex-column">
        <div class="all-works__head-container">
            <h1 class="all-works__head-title title">Mes réalisations</h1>
            <p class="all-works__head-subtitle subtitle">Vous retrouverez ci-dessous mes réalisations en développement web</p>
        </div>
        <div class="all-works__filter">
            <span id="filterTogg" class="all-works__filter-btn btn btn--small btn--mint btn--square">Filtres</div>
        </div>
    </div>
    <div id="tags" class="tag">
        <div class="tag__container">
            <div id="allTags" class="flex-row flex-row--start">
                <span class="tag__item btn btn--dark btn--xsmall filterBtn" onclick="filterSelection('All')">Tous</span>
                <span class="tag__item btn btn--mint btn--xsmall filterBtn" onclick="filterSelection('Projet')">Projets</span>
                <span class="tag__item btn btn--darkgreen btn--xsmall filterBtn" onclick="filterSelection('Fonctionnalitée')">Fonctionnalités</span>
                <?php foreach ($allTech as $value) { ?>
                    <span class="tag__item btn <?php echo getTechnologiesColors($value['name']); ?> btn--xsmall filterBtn" onclick="filterSelection('<?php echo $value['name']; ?>')"><?php echo $value['name'] ?></span>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="all-works__projects flex-row flex-row--start">
        <?php foreach ($allWorks as $value) { ?>
            <div class="project filterDiv <?php echo $value['categorie']; 
            foreach (getTechnologiesInfos($value['id']) as $tech) {
                echo ' ' .  $tech['name'] . ' ';
            } ?>">
                <div class="project__container">
                    <div class="project__head">
                        <img src="../../src/assets/images/works/thumbnail/<?php echo $value['thumbnail'] ?>" alt="Visuel du projet" class="project__thumbnail">
                        <div class="project__caption">
                            <div class="project__caption-bg"></div>
                            <div class="project__caption-content">
                                <a href="single-work.php?id=<?php echo $value['id'] ?>" class="project__caption-link"><i class="fas fa-eye project__caption-icon"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="project__categorie <?php echo getCategorieColor($value['categorie']); ?>">
                        <p class="project__categorie-name"><?php echo $value['categorie']; ?></p>
                    </div>
                    <div class="project__details">
                        <h3 class="project__details-title title"><?php echo $value['name']; ?></h3>
                        <p class="project__details-subtitle subtitle"><?php echo subMyString($value['subtitle'], 50, '...'); ?></p>
                        <div class="flex-row flex-row--center">
                            <?php foreach (getTechnologiesInfos($value['id']) as $tech) { ?>
                                <span class="project__details-tag btn <?php echo getTechnologiesColors($tech['name']); ?> btn--xsmall"><?php echo $tech['name']; ?></span>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</section>

<?php get_footer(); ?>