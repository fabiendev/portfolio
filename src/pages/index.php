<?php
require_once('../functions/index-functions.php');
get_header();
?>

<section class="hero">
    <h1 class="hero__title title title--primary">Développeur web & formateur</h1>
    <p class="hero__subtitle subtitle">
        Je suis passionné d'informatique.<br>
        Je code des applications web et je transmets ma passion en formant les personnes à l'utilisation des outils informatiques.
    </p>
    <div class="hero__btn flex-row">
        <a href="works.php" class="btn btn--mint">Mes réalisations</a>
        <a href="contact.php" class="btn btn--mint">Me contacter</a>
    </div>
</section>

<div class="picture">
    <img src="../../src/assets/images/mountain.png" alt="Visuel montagne" class="picture__item">
</div>

<section class="about">
    <div class="mint-block">
        <div class="container">
            <h2 class="about__title title">Bonjour, je suis Fabien. Bienvenue.</h2>
            <p class="about__description"> J'ai toujours eu un goût prononcé pour l'informatique que j'ai apprise en autodidacte. J'ai ensuite travaillé plusieurs années dans le support client et la formation pour des FAI et un extranet d'une agence web. En parallèle, je donne des cours pour former les personnes aux outils informatiques (OS, logiciels). 
            De là me vient mon goût pour la formation.<br>
            <br>
            Très intérressé par le développement web, j'ai effectué en 2019/2020, une formation de développeur web qui m'a permis d'apprendre différents langages de programmation front-end et back-end. J'ai obtenu en juillet 2020, le Titre professionnel de développeur web/web mobile.<br>
            <br>
            Je suis très motivé par l'apprentissage de nouvelles choses, naturellement curieux je travaille en permanence à améliorer mes compétences dans le développement web.</p>
            <div class="interests">
                <h3 class="interests__title title">Mes intérêts</h3>
                <div class="interests__container flex-row">
                    <div class="interests__item">
                        <i class="interests__icon fas fa-mountain"></i>
                        <p class="interests__name">Randonnée</p>
                    </div>
                    <div class="interests__item">
                        <i class="interests__icon fas fa-desktop"></i>
                        <p class="interests__name">Informatique</p>
                    </div>
                    <div class="interests__item">
                        <i class="interests__icon fas fa-headphones"></i>
                        <p class="interests__name">Musique</p>
                    </div>
                    <div class="interests__item">
                        <i class="interests__icon fas fa-heart"></i>
                        <p class="interests__name">Bénévolat</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="content-box">
    <section class="skills container">
        <div class="skills__container flex-row">
            <div class="skills__item">
                <h2 class="skills__title title">Développeur web</h2>
                <i class="skills__icon fas fa-laptop-code"></i>
                <p class="skills__subtitle">J'utilise différents langages et outils en voici quelques-uns :</p>
                <ul class="skills__details">
                    <li><strong>Design:</strong></li>
                    <li>Adobe XD, Lunacy, Photoshop.</li>
                    <li><strong>Front-end:</strong></li>
                    <li>HTML5, CSS3, SASS, Javascript.</li>
                    <li><strong>Back-end:</strong></li>
                    <li>PHP, SQL, phpMyAdmin, terminal.</li>
                    <li><strong>Outils de développement:</strong></li>
                    <li>Bootstrap, GIT, Gitlab, NPM, Composer.</li>
                    <li><strong>Librairie & API:</strong></li>
                    <li>Leaflet, OpenWeatherMap, Nominatim, Adresse.</li>
                </ul>
            </div>
            <div class="skills__item">
                <h2 class="skills__title title">Formateur</h2>
                <i class="skills__icon fas fa-book"></i>
            
                <div class="skills__details">
                    <h3><strong>Cadre des formations que j'ai animées :</strong></h3>
                    <p>Au sein d'une agence, dans les locaux d'entreprises, à domicile chez des particuliers, en vidéoconférences, j'ai animé diverses formations 
                    sur des systèmes d'exploitations, des logiciels et des extranets.</p>
                    <br>
                    <h3><strong>J'articulais mes formations de cette manière :</strong></h3>
                    <ul>
                        <li>-Evaluation des personnes à former.</li>
                        <li>-Construction d'une progression pédagogique.</li>
                        <li>-Création d'une dynamique pour susciter l’intérêt et l’implication des participants.</li>
                        <li>-Mesure de l’atteinte des objectifs.</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="featured-works container border-top">
        <div class="featured-works__head">
            <h2 class="featured-works__head-title title">Mes dernières réalisations</h2>
            <p class="featured-works__head-subtitle subtitle">Vous retrouverez ci-dessous quelques-unes de mes réalisations</p>
        </div>
        <div class="featured-works__projects flex-row flex-row--between">
            <?php foreach ($recentsWorks as $value) { ?>
                <div class="featured-project">
                    <img src="../../src/assets/images/works/thumbnail/<?php echo $value['thumbnail'] ?>" alt="Visuel du projet" class="featured-project__thumbnail">
                    <div class="featured-project__caption">
                    <div class="featured-project__caption-bg"></div>
                        <div class="featured-project__caption-container flex-column">
                        <h3 class="featured-project__caption-title title"><?php echo $value['name']; ?></h3>
                        <p class="featured-project__caption-subtitle"><?php echo $value['subtitle']; ?></p>
                        <a href="single-work.php?id=<?php echo $value['id'] ?>" class="featured-project__caption-btn btn btn--pink">Voir</a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="featured-works__more">
            <a href="works.php" class="featured-works__more-btn btn btn--mint">Voir tout</a>
        </div>
    </section>

    <section class="social container border-top">
        <div class="social__head">
            <h2 class="social__head-title title">En savoir plus</h2>
            <p class="social__head-subtitle subtitle">Vous pouvez consulter mon profil Gitlab et Linkedin</p>
        </div>
        <div class="social__container flex-column">
            <div class="social__item">
                <a href="https://gitlab.com/fabiendev" class="social__item-link flex-column">
                    <i class="social__item-gitlab fab fa-gitlab"></i>
                    <span class="social__item-title">Gitlab</span>
                </a>
            </div>
            <div class="social__item">
                <a href="https://www.linkedin.com/in/fabien-ducousso/" class="social__item-link flex-column">
                    <i class="social__item-linkedin fab fa-linkedin-in"></i>
                    <span class="social__item-title">Linkedin</span>
                </a>
            </div>
        </div>
    </section>
    <div class="contact-box container border-top">
        <section class="contact-home">
            <div class="contact-home__container">
                <div class="contact-home__item">
                    <h2 class="contact-home__item-title title">Travaillons ensemble</h2>
                </div>
                <div class="contact-home__item">
                    <p class="contact-home__item-subtitle subtitle">Je suis toujours intéressé pour discuter de projets.</p>
                </div>
                <div class="contact-home__box">
                    <a href="contact.php" class="contact-home__box-btn btn btn--pink btn--center">Contactez-moi</a>
                </div>
            </div>
        </section>
    </div>
</div>
  
<?php get_footer(); ?>