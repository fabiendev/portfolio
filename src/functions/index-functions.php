<?php
require_once('../includes/_dispacher.php');


// Récupère un tableau contenant toutes les informations des projets, catégories et technologies
function getRecentsWorks() {
    global $db;
    
    $sql = 'SELECT id, name, subtitle, thumbnail, created_date
    FROM projects
    WHERE featured = "yes"
    ORDER BY created_date asc LIMIT 6';
    $request = $db->query($sql);
    $result = $request->fetchAll();

    return $result;
}

$recentsWorks = getRecentsWorks();






