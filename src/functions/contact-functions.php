<?php 
require_once('../includes/_dispacher.php');

// Fontion d'envoi d'un email
function sendMail() {
    $myEmail="f.ducousso@protonmail.com";
    $error = [];
    $validation = true;

    if (empty($_POST['email']) || empty($_POST['subject']) || empty($_POST['message'])) {
        $error[] = 'Tous les champs sont obligatoires.';
        $validation = false;
    } 

    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $error[] = 'L\'adresse e-mail n\'est pas valide.';
        $validation = false;
    }

    if (isset($_POST['reason']) && !empty($_POST['reason'])) {
        $error[] = 'Votre message n’a pas été envoyé en raison d\'une erreur';
        $validation = false;
    }

    if ($validation) {
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=UTF-8\r\n";
        $headers .= "From: Nom de votre site <".$_POST['email'].">\r\n";
        $headers .= "Reply-To: Nom de votre site <".$_POST['email'].">\r\n";
        $email=$_POST['email']; 
        $subject='=?UTF-8?B?'.base64_encode($_POST['subject']).'?=';
        $message=htmlentities($_POST['message'],ENT_QUOTES,"UTF-8");
        mail($myEmail,$subject,nl2br($message),$headers);
    }
    return $error;
}
