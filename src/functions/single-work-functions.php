<?php
require_once('../includes/_dispacher.php');


// Récupère un tableau contenant toutes les informations d'un projet, en récupérant en GET l'ID du projet
function getSingleWork() {
    global $db;

    if (isset($_GET['id']) && $_GET['id'] != "" && (int)$_GET['id']) {
        $data['id'] = $_GET['id'];
        $sql = 'SELECT id, name, subtitle, description, categorie, thumbnail, gitlab, url
        FROM projects
        WHERE id = :id
        ORDER BY id';
        $request = $db->prepare($sql);
        $request->execute($data);
        $result = $request->fetchAll();

        return $result;

    } else {
        header('Location: ' . '404.php' );
    }
}

$singleWork = getSingleWork();


// Récupère les images en BDD en récupérant le projet via le GET ID
function getGallery() {
    global $db;

    if (isset($_GET['id']) && $_GET['id'] != "" && (int)$_GET['id']) {
        $data['id'] = $_GET['id'];
        $sql = 'SELECT id, name, url, project_name
        FROM pictures
        WHERE id_projects = :id
        ORDER BY id';
        $request = $db->prepare($sql);
        $request->execute($data);
        $result = $request->fetchAll();

        return $result;

    } else {
        return "Pas encore de capture d'écran";
    }
}

$gallery = getGallery();