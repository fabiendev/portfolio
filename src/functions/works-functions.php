<?php 
require_once('../includes/_dispacher.php');

// Récupère un tableau contenant toutes les informations des projets
function getAllWorks() {
    global $db;
    
    $sql = 'SELECT id, name, subtitle, categorie, thumbnail, created_date
    FROM projects
    ORDER BY created_date';
    $request = $db->query($sql);
    $result = $request->fetchAll();

    return $result;
}

$allWorks = getAllWorks();