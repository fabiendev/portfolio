'use strict'

let filterTogg = document.getElementById("filterTogg");
let tags = document.getElementById("tags");

filterTogg.addEventListener("click", () => {
    if(getComputedStyle(tags).display != "none"){
        tags.style.display = "none";
    } else {
        tags.style.display = "block";
    }
})