'use strict'

baguetteBox.run('.gallery', {
    animation: 'fadeIn',
    noScrollbars: true
});