'use strict'

filterSelection('All');

function filterSelection(tag) {
    let filter = document.getElementsByClassName('filterDiv');

    if (tag == 'All') {
        tag = '';
    }

    for (let i = 0; i < filter.length; i++) {
        removeClass(filter[i], 'show');

        if (filter[i].className.indexOf(tag) > -1) {
            addClass(filter[i], 'show');
        }
    }
}

// Ajoute la class "show"

function addClass(element, name) {
    let arr1, arr2;
    arr1 = element.className.split(" ");
    arr2 = name.split(" ");
  
    for (let i = 0; i < arr2.length; i++) {
      if (arr1.indexOf(arr2[i]) == -1) {
        element.className += " " + arr2[i];
      }
    }
  }
  
  // Supprime la class "show"
  
  function removeClass(element, name) {
    let arr1, arr2;
  
    arr1 = element.className.split(" ");
    arr2 = name.split(" ");
    for (let i = 0; i < arr2.length; i++) {
      while (arr1.indexOf(arr2[i]) > -1) {
        arr1.splice(arr1.indexOf(arr2[i]), 1);
      }
    }
    element.className = arr1.join(" ");
  }