<?php require_once('../../src/includes/_dispacher.php'); ?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../src/assets/css/main.css">
    <script defer src="../assets/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
    <title>Accueil</title>
</head>
<body>
    <header class="header">
        <div class="header__container flex-row flex-row--between">
            <div class="logo">
                <a href="../../src/pages/index.php" class="logo__link">
                    <img class="logo__picture" src="../../src/assets/images/logo.png" alt="Logo du site">
                </a>
            </div>
            <nav class="small-menu">
                <div class="small-menu__container container-full flex-row flex-row--center">
                    <div class="small-menu__item">
                        <a href="../../src/pages/works.php" class="small-menu__link">
                            <div class="small-menu__content flex-column">
                                <i class="small-menu__content-icon far fa-file-code"></i>
                                <p class="small-menu__content-title">Réalisations</p>
                            </div>
                        </a>
                    </div>
                    <div class="small-menu__item">
                        <a href="../../src/pages/contact.php" class="small-menu__link">
                            <div class="small-menu__content flex-column">
                                <i class="small-menu__content-icon far fa-envelope"></i>
                                <p class="small-menu__content-title">Contact</p>
                            </div>
                        </a>
                    </div>
                </div>
            </nav>
            <nav class="big-menu">
                <ul class="big-menu__container flex-row flex-row--end">
                    <li class="big-menu__item">
                        <a href="../../src/pages/works.php" class="big-menu__link">Réalisations</a>
                    </li>
                    <li class="big-menu__item">
                        <a href="../../src/pages/contact.php" class="big-menu__link">Contact</a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>
    <main>
