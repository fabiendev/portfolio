        </main>

        <footer class="footer">
            <div class="footer__background">
                <img src="../../src/assets/images/mountain-footer.png" alt="Visuel montagne" class="footer__picture">
            </div>
            <div class="footer__block">
                <div class="footer__items flex-column">
                    <div class="footer__logo">
                        <a href="https://fabiendev.com" class="footer__logo-link"><img class="footer__logo-picture" src="../../src/assets/images/logowhite.png" alt="Logo du site"></a>
                    </div>
                    <div class="footer__social flex-row">
                        <div class="footer__social-item">
                            <a href="https://gitlab.com/fabiendev" class="footer__social-link footer__social-link--gitlab"><i class="footer__social-icon fab fa-gitlab"></i></a>
                        </div>
                        <div class="footer__social-item">
                            <a href="https://www.linkedin.com/in/fabien-ducousso/" class="footer__social-link footer__social-link--linkedin"><i class="footer__social-icon fab fa-linkedin-in"></i></a>
                        </div>
                        <div class="footer__social-item">
                            <a href="mailto:f.ducousso@protonmail.com" class="footer__social-link"><i class="footer__social-icon far fa-envelope"></i></a>
                        </div>
                    </div>
                    <div class="footer__copyright">
                        <p><a href="https://fabiendev.com" class="footer__copyright-link">fabiendev.com</a> © 2021</p>
                    </div>
                </div>
            </div>
        </footer>
</body>
</html>