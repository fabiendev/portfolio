<?php

// Debug pour PDO
define('DEBUG', true);

// Informations de connexion à la BDD 
define('DB_HOST', 'localhost');
define('DB_NAME', 'portfolio_natif');
define('DB_USER', 'root');
define('DB_PASSWORD', '');

// Chemins stockés dans des constantes
define('ROOT', dirname(__DIR__));