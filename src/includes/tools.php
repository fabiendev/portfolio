<?php

// Améliore la lisibilité d'un var_dump
function dump($variable) {
	echo '<pre>';
	var_dump($variable);
	echo '</pre>';
}

// Fonction d'appel du header
function get_header() {
	require_once ROOT . '/layouts/header.php';
}


// Fonction d'appel du footer
function get_footer() {
	require_once ROOT . '/layouts/footer.php';
}

// Récupère un tableau contenant toutes les informations des technologies
function getAllTech() {
    global $db;
    
    $sql = 'SELECT id, name, description
    FROM technologies
    ORDER BY id';
    $request = $db->query($sql);
    $result = $request->fetchAll();

    return $result;
}

$allTech = getAllTech();

// Récupère les technologies d'un projet via son ID 
function getTechnologiesInfos($id) {
    global $db;

    $data['id'] = $id;

    $sql = 'SELECT technologies.name
    FROM projects_tech
    JOIN technologies
        ON id_technologies = technologies.id
    WHERE id_projects = :id';
    $request = $db->prepare($sql);
    $request->execute($data);
    $result = $request->fetchAll();

    return $result;
}

// Récupère les technologies d'un projet via son ID et renvoi le résultat groupé
function getTechnologiesInfosGroup($id) {
    global $db;

    $data['id'] = $id;

    $sql = 'SELECT GROUP_CONCAT(technologies.name SEPARATOR " " ) as technologiesGroup
    FROM projects_tech
    JOIN technologies
        ON id_technologies = technologies.id
    WHERE id_projects = :id';
    $request = $db->prepare($sql);
    $request->execute($data);
    $result = $request->fetchAll();

    return $result;
}

// Récupère la classe couleur de la catégorie pour le background color
function getCategorieColor($cat) {
    if ($cat === 'Fonctionnalité') {
        $result = 'bg-color bg-color--darkgreen';
    } else {
        $result = 'bg-color bg-color--mint';
    }
    return $result;
}

// Récupère la classe couleur de la catégorie pour la couleur du bouton
function getCategorieColorBtn($cat) {
    if ($cat === 'Fonctionnalité') {
        $result = 'label-btn--darkgreen';
    } else {
        $result = 'label-btn--mint';
    }
    return $result;
}

// Récupère la classe couleur de la technologie
function getTechnologiesColors($tech) {
    if ($tech === 'PHP') {
        $result = 'label-btn--php';
    } elseif ($tech === 'JS') {
        $result = 'label-btn--js';
    } elseif ($tech === 'SASS') {
        $result = 'label-btn--sass';
    } elseif ($tech === 'SQL') {
        $result = "label-btn--sql";
    } elseif ($tech === 'WP') {
        $result = "label-btn--wp";
    } elseif ($tech === 'API') {
        $result = "label-btn--api";
    } elseif ($tech === 'Vue.js' ) {
        $result = 'label-btn--vue';
    } elseif ($tech === 'Alpine.js') {
        $result = "label-btn--alpine";
    } else {
        $result = 'label-btn--mint';
    }
    return $result;
}

// Permet de couper un texte à un certain nombre de caractères sans couper les mots.
function subMyString($content, $limit, $separator) {
    if(strlen($content) >= $limit ) {
        $content = substr($content, 0, $limit );
        $content = substr($content, 0, strrpos($content, ' ') );
        $content .= $separator;
    }
    return $content;
}